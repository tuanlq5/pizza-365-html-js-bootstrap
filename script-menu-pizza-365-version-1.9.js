    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gComboSize = {};
    var gPizza = "";
    var gObjectOrder = {};
    var gComboSizeSmall = "S";
    var gComboSizeMedium= "M";
    var gComboSizeLarge = "L";
    var gPizzaHaiSan = "Pizza Ocean Mania";
    var gPizzaHawai = "Pizza Hawaiian";
    var gPizzaBacon = "Pizza Chicken Bacon" ;
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    const gBASE_URL_VOUCHER = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/"
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        $(document).ready(function(){
            //gán sự kiện khi tải trang
            onPageLoading();
            // gán sự kiện cho nút chon size small
            $("#btn-chon-size-small").on("click",function(){
                onBtnChonSizeSmall();
            })
            // gán sự kiện cho nút chon size medium
            $("#btn-chon-size-medium").on("click",function(){
                onBtnChonSizeMedium();
            })
            // gán sự kiện cho nút chon size large
            $("#btn-chon-size-large").on("click",function(){
                onBtnChonSizeLarge();
            })
            // gán sự kiện cho nút chọn pizza mania
            $("#btn-chon-pizza-hai-san").on("click",function(){
                onBtnChonPizzaHaiSan();
            })
            // gán sự kiện cho nút chọn pizza hawaiian
            $("#btn-chon-pizza-hawaii").on("click",function(){
                onBtnChonPizzaHawaiian();
            })
            // gán sự kiện cho nút chọn pizza  bacon
            $("#btn-chon-pizza-bacon").on("click",function(){
                onBtnChonPizzaBacon();
            })
            // gán sự kiện cho nút gửi đơn
            $("#btn-send-order").on("click", function(){
                onBtnGuiOrderClick();
            })
            // gán sự kiện cho nút ấn tạo đơn 
            $("#btn-modal-create-order").on("click", function(){
                onBtnCreateOrderInModalClick();
            })
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        // hàm xử lý sự kiện khi tải trang
        function onPageLoading(){
            loadDataDrinkToSelect();
        }
        // hàm xử lý khi ấn nút chọn size small
        function onBtnChonSizeSmall(){
            // thêm combo vào biến toàn cục
            gComboSize = getSizePizza(gComboSizeSmall, "20cm", 2, "200g", 2, "150000");
            // thay đổi màu nút khi được chọn
            changeButtonComboColor(gComboSizeSmall);
            gComboSize. displayInConsoleLog();
        }
        // hàm xử lý khi ấn nút chọn size medium
        function onBtnChonSizeMedium(){
            // thêm combo vào biến toàn cục
            gComboSize = getSizePizza(gComboSizeMedium, "25cm", 4, "300g", 3, "200000");
            // thay đổi màu nút khi được chọn
            changeButtonComboColor(gComboSizeMedium);
            gComboSize.displayInConsoleLog();
        }
        // hàm xử lý khi ấn nút chọn size large
        function onBtnChonSizeLarge(){
            // thêm combo vào biến toàn cục
            gComboSize = getSizePizza(gComboSizeLarge, "30cm", 8, "500g", 4, "250000");
            // thay đổi màu nút khi được chọn
            changeButtonComboColor(gComboSizeLarge);
            gComboSize.displayInConsoleLog();
        }
        // hàm xử lý khi nút chọn pizza mania được ấn 
        function onBtnChonPizzaHaiSan(){
            // thêm pizza được chọn vào biến toàn cục
            gPizza = gPizzaHaiSan;
            // thay đổi màu nút khi được chọn
            changeButtonPizza(gPizzaHaiSan);
            console.log("%cPizza được chọn :" + gPizza ,"color:cyan");
        }
        // hàm xử lý khi nút chọn pizza hawaiian được ấn 
        function onBtnChonPizzaHawaiian(){
            // thêm pizza được chọn vào biến toàn cục
            gPizza = gPizzaHaiSan;
            // thay đổi màu nút khi được chọn
            changeButtonPizza(gPizzaHawai);
            console.log("%cPizza được chọn :" + gPizza ,"color:cyan");
        }
        // hàm xử lý khi nút chọn pizza chicken bacon được ấn 
        function onBtnChonPizzaBacon(){
            // thêm pizza được chọn vào biến toàn cục
            gPizza = gPizzaBacon;
            // thay đổi màu nút khi được chọn
            changeButtonPizza(gPizzaBacon);
            console.log("%cPizza được chọn :" + gPizza ,"color:cyan");
        }
        // hàm xử lý khi ấn nút gửi đơn
        function onBtnGuiOrderClick(){
            // bước 1: đọc gán dữ liệu
            gObjectOrder = docGanDuLieuNutGuiDon();
            console.log(gObjectOrder);
            // bước 2: kiểm tra dữ liệu
            if(validateInputGuiOrder(gObjectOrder)){
            // bước 3: xử lý hiển thị
            hienThiModalGuiOrder();    
            }
        }
        // hàm xử lý khi ấn nút tạo đơn
        function onBtnCreateOrderInModalClick(){
            // bước 1: đọc gán dữ liệu
            var vObject = readDataToObject();
            // bước 2: kiểm tra dữ liệu(không cần )
            // bước 3: gửi request tới API
            $.ajax({
                type:"POST",
                url: gBASE_URL,
                contentType: "application/json;charset=UTF-8",
                data: JSON.stringify(vObject),
                success: function(data){
                    console.log(data);
                    xuLyHienThiKhiOrderSuccess(data.orderCode);   
                    
                },
                error: function(error){
                    console.assert(error.responseText);
                }
            })
          
            
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        /** hàm đọc gán dữ liệu nút gửi đơn 
         * input được nhập trên form
         * output 1 object được gán dữ liệu
         */function docGanDuLieuNutGuiDon(){
            // tạo object có dữ liệu rỗng
            var vObject = {
                fullName:"",
                email:"",
                soDienThoai:"",
                diaChi:"",
                loiNhan:"",
                loaiDoUong:"",
                menuDuocChon: gComboSize,
                pizza: gPizza,
                voucher: "",
                priceVND: gComboSize.priceVND,
                discountPercent:-1,
                priceClientPay: function(){
                return (this.priceVND*(100 - this.discountPercent)/100); // tính giá tiền cho khách hàng nếu có discount
                }
            }
            vObject.fullName = $("#inp-fullname").val();
            vObject.email = $("#inp-email").val();
            vObject.soDienThoai = $("#inp-phone-number").val();
            vObject.diaChi = $("#inp-address").val();
            vObject.loiNhan = $("#inp-message").val();
            vObject.voucher = $("#inp-discount").val();
            vObject.loaiDoUong = $("#sel-drink").val();
            if(vObject.voucher === ""){
                vObject.discountPercent = 0;
            }else{
                $.ajax({
                    async: false,
                    type:"GET",
                    url: gBASE_URL_VOUCHER + vObject.voucher,
                    success: function(data){
                        vObject.discountPercent = data.phanTramGiamGia;
                        console.log(data);
                       
                    },
                    error: function(error){
                        vObject.discountPercent = 0;
                        vObject.voucher ="";
                    }
                })
            }
            console.log(vObject);
            return vObject;
        }
        /** hàm thêm combo vào biến toàn cục
         * input: thong tin combo
         * output: trả về 1 biến object có 1 method show thông tin lên console log
         */ function getSizePizza(paramCombo, paramDuongKinh, paramSuon, paramSalad, paramNuocNgot, paramPriceVND){
            var vNewComboSize = {
                comboSize: paramCombo,
                duongKinhPizza: paramDuongKinh,
                suon: paramSuon,
                salad: paramSalad,
                nuocNgot: paramNuocNgot,
                priceVND: paramPriceVND,
                displayInConsoleLog (){
                    console.log("%cChi tiết Combo đã chọn:" ,"color: cyan");
                    console.log("Combo size:" + this.comboSize);
                    console.log("Đương kính pizza:" + this.duongKinhPizza);
                    console.log("Sườn nướng:" + this.suon);
                    console.log("Salad:" + this.salad);
                    console.log("Nước ngọt: " + this.nuocNgot);
                    console.log("Price VND: " + this.priceVND);
                } 
            }; 
            return vNewComboSize;
        }
        /** hàm thay đổi màu nút khi ấn nút chon combo
         * input: khi click
         * output: thay đổi màu nút
         */ function changeButtonComboColor(paramCombo){
            if(paramCombo === gComboSizeSmall){
                $("#btn-chon-size-small").attr("class","btn btn-danger w-100");
        
                $("#btn-chon-size-medium").attr("class","btn btn-warning w-100");

                $("#btn-chon-size-large").attr("class","btn btn-warning w-100");
            }
            if(paramCombo === gComboSizeMedium){
                $("#btn-chon-size-small").attr("class","btn btn-warning w-100");
        
                $("#btn-chon-size-medium").attr("class","btn btn-danger w-100");

                $("#btn-chon-size-large").attr("class","btn btn-warning w-100");
            }
            if(paramCombo === gComboSizeLarge){
                $("#btn-chon-size-small").attr("class","btn btn-warning w-100");
        
                $("#btn-chon-size-medium").attr("class","btn btn-warning w-100");

                $("#btn-chon-size-large").attr("class","btn btn-danger w-100");
            }
        }
        /** hàm thay đổi màu nút khi ấn nút chọn pizza
     * input: khi click
     * output: thay đổi màu nút
     */function changeButtonPizza(paramPizza){
        if(paramPizza === gPizzaHaiSan){
            $("#btn-chon-pizza-hai-san").attr("class","btn btn-danger w-100");

            $("#btn-chon-pizza-hawaii").attr("class","btn btn-warning w-100");

            $("#btn-chon-pizza-bacon").attr("class","btn btn-warning w-100");
        }
        if(paramPizza === gPizzaHawai){
            $("#btn-chon-pizza-hai-san").attr("class","btn btn-warning w-100");

            $("#btn-chon-pizza-hawaii").attr("class","btn btn-danger w-100");

            $("#btn-chon-pizza-bacon").attr("class","btn btn-warning w-100");
        }
        if(paramPizza === gPizzaBacon){
            $("#btn-chon-pizza-hai-san").attr("class","btn btn-warning w-100");

            $("#btn-chon-pizza-hawaii").attr("class","btn btn-warning w-100");

            $("#btn-chon-pizza-bacon").attr("class","btn btn-danger w-100");
        }
        }
        // hàm load data vào thẻ select
        function loadDataDrinkToSelect(){
        // goij ajax lấy thông tin nước uống
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
            type: "GET",
            success: function(data){
                console.log(data);
                // dung loop để bỏ lên ô select
                for(var bI = 0 ; bI < data.length ; bI++){
                    $("#sel-drink").append($("<option>").val(data[bI].maNuocUong).text(data[bI].tenNuocUong));
                }
            },
            error: function(error){
                console.assert(error.responseText);
            }
        })
        }
        // hàm load thông tin voucher nêu có 
        function loadDataVoucher(paramVoucher, paramDiscountPercent){
        $.ajax({
            async: false,
            type:"GET",
            url: gBASE_URL_VOUCHER + paramVoucher,
            success: function(data){
                paramDiscountPercent = data.phanTramGiamGia;
                console.log(data);
                console.log(paramDiscountPercent);
            },
            error: function(error){
                paramDiscountPercent = 0;
            }
        })
        }
        // hàm kiểm tra dữ liệu nút gửi order
        function validateInputGuiOrder(paramObject){
            if(Object.keys(paramObject.menuDuocChon).length === 0){ // check combo
                alert("Bạn chưa chọn combo!");
                return false;
            } if(paramObject.pizza === ""){ //check loại pizza
                alert("Bạn chưa chọn lại pizza!");  
                return  false;
            }if(paramObject.loaiDoUong === "all"){ // check loại đồ uống
                alert("Bạn chọn đồ uống!");
                return false;
            }if(paramObject.fullName === ""){ // check họ tên
                alert("Bạn chưa điền họ và tên!");
                return false;
            } if(paramObject.email === ""){ // check email
                alert("Bạn chưa điền email!");
                return false;
            }//if(KiemTraInputEmail(paramObject.email) == false){ // check email
               // alert("Email không khả dụng!");
              //  return false;
            //}if(validatePhoneCall(paramObject.soDienThoai) == false){ //check số điện thoại
              //  alert("Số điện thoại không khả dụng!");
               // return false;
            //} if(paramObject.diaChi === ""){ //check địa chỉ
               // alert("Bạn chưa điền địa chỉ!");
               // return false;
            //}
            return true;
        }
        // function kiểm tra email 
        // đầu vào: giá trị email
        // đầu ra: return true nếu email đúng định dạng, return false nếu email không đúng định dạng
        function KiemTraInputEmail(paramEmail){
                var vValidRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (paramEmail.match(vValidRegex)) { 
                return true;
            } else {
                return false;
            }
        }
        // hàm kiểm tra định dạng số điện thoại
        // đầu vào là số điện thoại
        // return true nếu là dãy số, false nếu không đúng
        function validatePhoneCall(paramPhoneNumber){
            var vSoDienThoai = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
            if(paramPhoneNumber.match(vSoDienThoai)){
            return true;
            }else {
            return false;
            }
        }
        /*  hàm xử lý hiển thị nút gửi đơn 
        *   input : thông tin từ form
        *   output : ghi thông tin lên modal
        */ function hienThiModalGuiOrder(){
            // thêm dữ liệu vào form trên modal
            $("#modal-orders-detail").modal("show");
            $("#input--modal-fullname").val(gObjectOrder.fullName);
            $("#input-modal-phone-number").val(gObjectOrder.soDienThoai);
            $("#input-modal-address").val(gObjectOrder.diaChi);
            $("#input-modal-message").val(gObjectOrder.loiNhan);
   
            if(gObjectOrder.discountPercent != 0 ){
                $("#input-modal-discount").val(gObjectOrder.voucher);
            }else{ $("#input-modal-discount").val("Voucher không khả dụng!");}
            // thêm dữ liệu vào thông tin chi tiết
            $("#textarea-modal-order-detail").val("Họ và tên: " + gObjectOrder.fullName + "."+ "\n"
                                                + "Địa chỉ: " +  gObjectOrder.diaChi   + "."+ "\n"
                                                + "Combo size : " +  gObjectOrder.menuDuocChon.comboSize + ", " + "sườn :" + gObjectOrder.menuDuocChon.suon + ", " + "đường kính pizza :" + gObjectOrder.menuDuocChon.duongKinhPizza + ", " + "salad :" + gObjectOrder.menuDuocChon.salad + ", " + "nước ngọt :" + gObjectOrder.menuDuocChon.nuocNgot  + "." +"\n"
                                                + "Loại Pizza: " +  gObjectOrder.pizza + "." + "\n"
                                                + "giá: " + gObjectOrder.priceVND + "." + "\n" 
                                                + "Mã giảm giá: " + gObjectOrder.voucher + "." + "\n"
                                                + "Tiền phải thanh toán: " +  gObjectOrder.priceClientPay()  + "(giảm giá" + gObjectOrder.discountPercent + "%)" + "." + "\n");
        }
        /** hàm đọc gán dữ liệu nút tạo đơn trên modal
         *  input: thông tin trên biến glodal
         *  output: object
         * */function readDataToObject(){
                // tạo 1 object empty
                var vObjectRequest = {
                    kichCo: "",
                    duongKinh: -1,
                    suon: -1,
                    salad: -1,
                    loaiPizza: "",
                    idVourcher: "",
                    idLoaiNuocUong: "",
                    soLuongNuoc: -1,
                    hoTen: "",
                    thanhTien: -1,
                    email: "",
                    soDienThoai: "",
                    diaChi: "",
                    loiNhan: ""
                };
                // gán dữ liệu vào object
                vObjectRequest.kichCo = gObjectOrder.menuDuocChon.comboSize;
                vObjectRequest.duongKinh = gObjectOrder.menuDuocChon.duongKinhPizza;
                vObjectRequest.suon = gObjectOrder.menuDuocChon.suon;
                vObjectRequest.salad = gObjectOrder.menuDuocChon.salad;
                vObjectRequest.loaiPizza = gObjectOrder.pizza;
                vObjectRequest.idVourcher = gObjectOrder.voucher;
                vObjectRequest.idLoaiNuocUong = gObjectOrder.loaiDoUong;
                vObjectRequest.soLuongNuoc = gObjectOrder.menuDuocChon.nuocNgot;
                vObjectRequest.hoTen = gObjectOrder.fullName;
                vObjectRequest.thanhTien = gObjectOrder.priceClientPay();
                vObjectRequest.email = gObjectOrder.email;
                vObjectRequest.soDienThoai = gObjectOrder.soDienThoai;
                vObjectRequest.diaChi = gObjectOrder.diaChi;
                vObjectRequest.loiNhan = gObjectOrder.loiNhan;
                // trả về object đã gán
                return vObjectRequest;
         }
        // hàm xử lý hiển thị khi ấn tạo đơn thành công
        function xuLyHienThiKhiOrderSuccess(paramOrderCode){
            // xóa thông tin vừa order
            $("#input--modal-fullname").val("");
            $("#input-modal-phone-number").val("");
            $("#input-modal-address").val("");
            $("#input-modal-message").val("");
            $("#input-modal-discount").val("");
            $("#textarea-modal-order-detail").val("");
            // hiện modal tạo order thành công kèm mã đơn hàng
            $("#input-modal-madonhang").val(paramOrderCode);
            $('#modal-orders-detail').modal('hide');
            
           
            $('.modal-backdrop fade').remove();
            // Load up a new modal...
            $('#modal-orders-success').modal('show');
        }

       